package de.mekaso.winservice.backend;

import de.mekaso.winservice.data.Document;
import java.util.List;

public interface DocumentService {
    public List<Document> findAllDocuments();
    public Document findById(Long id);
}

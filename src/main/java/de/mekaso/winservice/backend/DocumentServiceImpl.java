package de.mekaso.winservice.backend;

import de.mekaso.winservice.data.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

@Service
public class DocumentServiceImpl implements DocumentService {

    private List<Document> allDocuments = null;
    
    @PostConstruct
    public void init() {
        allDocuments = new ArrayList();
        Document document = new Document();
        document.setId(Long.valueOf(1));
        document.setTitle("Gone with the Wind");
        document.setFilename("gone.pdf");
        allDocuments.add(document);
        document = new Document();
        document.setId(Long.valueOf(2));
        document.setTitle("java for Dummies");
        document.setFilename("jfd.docx");
        allDocuments.add(document);
        document = new Document();
        document.setId(Long.valueOf(3));
        document.setTitle("Microservices for Beginners");
        document.setFilename("microservices.pdf");
        allDocuments.add(document);
    }
    
    @Override
    public List<Document> findAllDocuments() {
        return this.allDocuments;
    }

    @Override
    public Document findById(Long id) {
        return this.allDocuments.stream().filter(document -> 
                Objects.equals(document.getId(), id)).findAny().orElse(null);
    }
}
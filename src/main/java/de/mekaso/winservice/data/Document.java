package de.mekaso.winservice.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ka7
 */
public class Document {
    private Long id;
    private String title;
    private String filename;
    
    @JsonProperty
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}

package de.mekaso.winservice;

import de.mekaso.winservice.data.Document;
import de.mekaso.winservice.backend.DocumentService;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Path("/documents")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class DocumentResource {
    @Autowired
    private DocumentService documentService;
    
    @GET
    public List<Document> findAllDocuments() {
        return this.documentService.findAllDocuments();
    }
    
    @GET
    @Path("/{id}")
    public Document findDocumentById(@PathParam("id") String idParam) {
        Long id = Long.parseLong(idParam);
        return this.documentService.findById(id);
    }
}
package de.mekaso.winservice;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ServiceApplication extends Application<ServiceConfiguration> {
    private static final String SERVICE_NAME = "Document-Service";
    
    public static void main(String[] args) throws Exception {
        new ServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return SERVICE_NAME;
    }

    @Override
    public void initialize(Bootstrap<ServiceConfiguration> bootstrap) {

    }

    @Override
    public void run(ServiceConfiguration configuration,
                    Environment environment) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan(this.getClass().getPackage().getName());
        context.refresh();
        environment.jersey().register(context.getBean(DocumentResource.class));
        environment.healthChecks().register("find-all", context.getBean(ServiceHealthCheck.class));
    }
}
package de.mekaso.winservice;

import com.codahale.metrics.health.HealthCheck;
import de.mekaso.winservice.data.Document;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceHealthCheck extends HealthCheck {

    @Autowired
    private DocumentResource documentResource;
    
    @Override
    protected Result check() throws Exception {
        Result result = Result.healthy("finding all documents works well");
        List<Document> allDocuments = null;
        try {
            allDocuments = this.documentResource.findAllDocuments();
            if (Objects.isNull(allDocuments) || allDocuments.isEmpty()) {
                result = Result.unhealthy("there are no documents");
            }    
        } catch(RuntimeException rte) {
            result = Result.unhealthy(rte);
        }
        return result;
    }
}